import React, {useContext, useEffect} from "react";
import {Link} from 'react-router-dom'
import {GithubContext} from "../context/github/githubContext";
import {Repos} from "../components/Repos";

export const Profile = ({match}) => {
    const {getUser, getRepos, loading, user, repos} = useContext(GithubContext);
    const urlName = match.params.name;

    useEffect(() => {
        getUser(urlName);
        getRepos(urlName)
        // eslint-disable-next-line
    }, []);

    if (loading) {
        return <p className="text-center">Загрузка...</p>
    }

    const {
        name, company, avatar_url,
        location, bio, blog, login,
        html_url, followers, public_repos,
        public_gists, following
    } = user;

    console.log(match);
    return (
        <>
            <Link to="/" className="btn btn-link"></Link>

            <div className="card">
                <div className="card-body">
                    <div className="row">
                        <div className="col-sm-5 text-center">
                            <img src={avatar_url}
                                 alt={name}
                                 style={{width: '150px'}}
                            />
                            <h1>{name}</h1>
                            {
                                location && <p>Местоположение: {location}</p>
                            }
                        </div>
                        <div className="col">
                            {
                                bio && <>
                                    <h3>BIO</h3>
                                    <p>{bio}</p>
                                </>
                            }
                            <a
                                href={html_url}
                                target="_blank"
                                rel="noreferrer noopener"
                                className="btn btn-dark mb-2"
                            >Открыть профиль</a>
                            <ul>
                                {login && <li>
                                    <strong>User name: </strong> {login}
                                </li>}
                                {company && <li>
                                    <strong>Company: </strong> {company}
                                </li>}
                                {blog && <li>
                                    <strong>Website: </strong> {blog}
                                </li>}
                            </ul>

                            <div className="badge badge-primary mr-1">
                                Подписчики: {followers}
                            </div>
                            <div className="badge badge-success mr-1">
                                Подписан: {following}
                            </div>
                            <div className="badge badge-info mr-1">
                                Репозитории: {public_repos}
                            </div>
                            <div className="badge badge-dark">
                                Gists: {public_gists}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Repos repos={repos}/>
        </>
    )
};
